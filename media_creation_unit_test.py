# -*- coding: utf-8 -*-
"""
Created on Thu Sep 11 16:12:59 2014

@author: mir.henglin
"""
# Python program that can be executed to report whether particular
# python packages are available on the system.

import os
import math
import sys
import marketingevolution.mediacreation as cre
import marketingevolution.simplefunctions as sf
import pandas as pd
import numpy as np
import nose
import unittest
import pandas.util.testing as tm
from pandas.util.testing import makeCustomDataframe as mkdf


def test_apply_decay_rate():

    x = pd.Series([0,1,2]) 
    y = pd.Series([3,4,5]) 
    z = pd.Series([6,7,8])
    
    exposures = [x, y, z]
    lags = [x, y, z]
    decay_rate = 0.5  
    
    expected_output = pd.Series([1, 0.781250, 0.179688])
    
    output = cre.apply_decay_rate(exposures, lags, decay_rate)
    
    assert tm.assert_almost_equal(output, expected_output)
    
    
    
def test_calc_decayed_exposures():
    x = pd.Series([0, 1, 2]) 
    y = pd.Series([3, 4, 5]) 
    z = pd.Series([6, 7, 8])
    
    exposures = [x, y, z]
    lags = [x, y, z]
    decay_rates = pd.Series([0, 0.33,1])  #  0.66, 
    
    expected_row_1 = pd.Series([3.0, 12.0, 21.0])
    expected_row_2 = pd.Series([1.5678, 2.383396, 1.291854])
    # expected_row_3 = pd.Series([0.5712, 0.194083, 0.014374])
    expected_row_4 = pd.Series([0.0, 0.0, 0.0])
    expected_output = pd.concat([expected_row_1, expected_row_2,  expected_row_4], axis=1).transpose() #expected_row_3,
    
    output = cre.calc_decayed_exposures(exposures, lags, decay_rates)
    
    # tm.assert_frame_equal(output, expected_output)
    assert tm.assert_almost_equal(output, expected_output)
    
   
def test_homogenzie_series_length():
    x = pd.Series([0.0, 1.0, 2.0]) 
    y = pd.Series([3.0, 4.0, 5.0, 6.0]) 
    z = pd.Series([7.0, 8.0, 9.0, 10.0, 11.0])
    
    x_expec = pd.Series([0.0, 1.0, 2.0, 0.0, 0.0]) 
    y_expec = pd.Series([3.0, 4.0, 5.0, 6.0, 0.0]) 
    z_expec = pd.Series([7.0, 8.0, 9.0, 10.0, 11.0])
    
    obs_exposures = [x,y,z]
    pro_exposures = [x,y,z]
    obs_days_before = [x,y,z]
    pro_days_before = [x,y,z]
    
    obs_exposures_expec = [x_expec,y_expec,z_expec]
    pro_exposures_expec = [x_expec,y_expec,z_expec]
    obs_days_before_expec = [x_expec,y_expec,z_expec]
    pro_days_before_expec = [x_expec,y_expec,z_expec]
    
    expected_output = (obs_exposures_expec, pro_exposures_expec, obs_days_before_expec, pro_days_before_expec)
    output = cre.homogenize_series_length(obs_exposures, pro_exposures, obs_days_before, pro_days_before)
    
    # outputs_are_equal = [tm.assert_almost_equal(out_series, expec_series) for (out_series, expec_series) in zip(out, expec) for (out, expec) in zip(output, expected_output)]
    assert tm.assert_almost_equal(output, expected_output)
    
def test_create_nondecayed_exposures():
    x = pd.Series([0,1,2]) 
    y = pd.Series([3,4,5]) 
    z = pd.Series([6,7,8])
    
    exposures = [x, y, z]
    lags = [x, y, z]

    
    expected_output = pd.Series([3.0, 12.0, 21.0])
    
    output = cre.create_nondecayed_exposures(exposures, lags)
    
    assert tm.assert_almost_equal(output, expected_output)
    
def test_calc_exposure_with_numeric_DMA():
    ms_dma_col = 'mediasheet_dma'
    dma_col = 'dma'
    ms_scaling_cols = ['mediasheet_scaling_1', 'mediasheet_scaling_2']
    ms_nads_col = 'mediasheet_number_of_ads'
    ms_date_col = 'mediasheet_date'
    date_col = 'date'
    
    r = {'dma' : pd.Series([100]),
         'scaling_1_var_1' : pd.Series([0.5]),
         'scaling_1_var_2' : pd.Series([0.6]),
         'scaling_2_var_1' : pd.Series([0.7]),
         'scaling_2_var_2' : pd.Series([0.8]),
         'date' : pd.Series([pd.to_datetime('01/02/2000')])}
    
    m = {'mediasheet_dma' : pd.Series([100, 100, 100, 200]),
         'mediasheet_scaling_1' : pd.Series(['scaling_1_var_1', 'scaling_1_var_2', 'scaling_1_var_1', 'scaling_1_var_2']),
         'mediasheet_scaling_2' : pd.Series(['scaling_2_var_1', 'scaling_2_var_2', 'scaling_1_var_1', 'scaling_1_var_2']),
         'mediasheet_number_of_ads' : pd.Series([1.0, 1.0, 1.0, 1.0]),
         'mediasheet_date' : pd.Series([pd.to_datetime('01/01/2000'), pd.to_datetime('01/02/2000'), pd.to_datetime('01/03/2000'), pd.to_datetime('01/04/2000')])}
    
    respondent = pd.DataFrame(r).iloc[0]
    media_sheet = pd.DataFrame(m)
    
    # One value for each unique date within qualifying DMAs Here three rows in the media sheet 
    # have a DMA that matches the respondent, and there are three unique dates within those three rows, 
    # so there are three values
    
    expec_o_exp = pd.Series([0.5*0.7, 0.6*0.8, 0.0])
    expec_p_exp = pd.Series([0.5*0.7, 0.6*0.8, 0.5*0.5])
    expec_o_days_before = pd.Series([1.0, 0.0, 0.0])
    expec_p_days_before = pd.Series([3.0, 2.0, 1.0])  
    
    expected_output = (expec_o_exp, expec_p_exp, expec_o_days_before, expec_p_days_before)
    output = cre.calc_exposure(respondent, media_sheet, ms_dma_col, dma_col, ms_scaling_cols, ms_nads_col, ms_date_col, date_col)
    
    outputs_are_equal = [tm.assert_almost_equal(out_series, expec_series) for (out_series, expec_series) in zip(output, expected_output)]
    assert all(outputs_are_equal)

def test_create_exposure_components_with_numeric_DMA(): 
    ms_dma_col = 'mediasheet_dma'
    dma_col = 'dma'
    ms_scaling_cols = 'mediasheet_scaling_1, mediasheet_scaling_2'
    ms_nads_col = 'mediasheet_number_of_ads'
    ms_date_col = 'mediasheet_date'
    date_col = 'date'
    
    d = {'dma' : pd.Series([100, 200]),
         'scaling_1_var_1' : pd.Series([0.5, 0.5]),
         'scaling_1_var_2' : pd.Series([0.6, 0.5]),
         'scaling_2_var_1' : pd.Series([0.7, 0.5]),
         'scaling_2_var_2' : pd.Series([0.8, 0.5]),
         'date' : pd.Series([pd.to_datetime('01/02/2000'), pd.to_datetime('01/05/2000')])}
    
    m = {'mediasheet_dma' : pd.Series([100, 100, 100, 200]),
         'mediasheet_scaling_1' : pd.Series(['scaling_1_var_1', 'scaling_1_var_2', 'scaling_1_var_1', 'scaling_1_var_2']),
         'mediasheet_scaling_2' : pd.Series(['scaling_2_var_1', 'scaling_2_var_2', 'scaling_2_var_1', 'scaling_2_var_2']),
         'mediasheet_number_of_ads' : pd.Series([1.0, 1.0, 1.0, 1.0]),
         'mediasheet_date' : pd.Series([pd.to_datetime('01/01/2000'), pd.to_datetime('01/02/2000'), pd.to_datetime('01/03/2000'), pd.to_datetime('01/04/2000')])}
    
    data = pd.DataFrame(d)
    media_sheet = pd.DataFrame(m)
    
    expec_o_exp = [pd.Series([0.5*0.7, 0.6*0.8, 0.0]), pd.Series([0.5*0.5, 0.0, 0.0])]
    expec_p_exp = [pd.Series([0.5*0.7, 0.6*0.8, 0.7*0.5]), pd.Series([0.5*0.5, 0.0, 0.0])]
    expec_o_days_before = [pd.Series([1.0, 0.0, 0.0]), pd.Series([1.0, 0.0, 0.0])]
    expec_p_days_before = [pd.Series([3.0, 2.0, 1.0]), pd.Series([0.0, 0.0, 0.0])]
    
    expected_output = (expec_o_exp, expec_p_exp, expec_o_days_before, expec_p_days_before)
    output = cre.create_exposure_components(media_sheet, data, dma_col, date_col, ms_dma_col, ms_date_col, ms_scaling_cols, ms_nads_col)

    assert tm.assert_almost_equal(output, expected_output)
    
def test_create_exposure_components_with_numeric_and_national_DMA(): 
    ms_dma_col = 'mediasheet_dma'
    dma_col = 'dma'
    ms_scaling_cols = 'mediasheet_scaling_1, mediasheet_scaling_2'
    ms_nads_col = 'mediasheet_number_of_ads'
    ms_date_col = 'mediasheet_date'
    date_col = 'date'
    
    d = {'dma' : pd.Series([100, 200]),
         'scaling_1_var_1' : pd.Series([0.5, 0.5]),
         'scaling_1_var_2' : pd.Series([0.6, 0.5]),
         'scaling_2_var_1' : pd.Series([0.7, 0.5]),
         'scaling_2_var_2' : pd.Series([0.8, 0.5]),
         'date' : pd.Series([pd.to_datetime('01/02/2000'), pd.to_datetime('01/05/2000')])}
    
    m = {'mediasheet_dma' : pd.Series([100, 100, 100, 200, 'National']),
         'mediasheet_scaling_1' : pd.Series(['scaling_1_var_1', 'scaling_1_var_2', 'scaling_1_var_1', 'scaling_1_var_2', 'scaling_1_var_1']),
         'mediasheet_scaling_2' : pd.Series(['scaling_2_var_1', 'scaling_2_var_2', 'scaling_2_var_1', 'scaling_2_var_2', 'scaling_2_var_1']),
         'mediasheet_number_of_ads' : pd.Series([1.0, 1.0, 1.0, 1.0, 1.0]),
         'mediasheet_date' : pd.Series([pd.to_datetime('01/01/2000'), pd.to_datetime('01/02/2000'), pd.to_datetime('01/03/2000'), pd.to_datetime('01/04/2000'), pd.to_datetime('01/01/2000')])}
    
    data_temp = pd.DataFrame(d)
    media_sheet = pd.DataFrame(m)
   
    # media_sheet['mediasheet_dma']
   
    expec_o_exp = [pd.Series([0.5*0.7*2, 0.6*0.8, 0.0]), pd.Series([0.5*0.5, 0.5*0.5, 0.0])]
    expec_p_exp = [pd.Series([0.5*0.7*2, 0.6*0.8, 0.7*0.5]), pd.Series([0.5*0.5, 0.5*0.5, 0.0])]
    expec_o_days_before = [pd.Series([1.0, 0.0, 0.0]), pd.Series([4.0, 1.0, 0.0])]
    expec_p_days_before = [pd.Series([3.0, 2.0, 1.0]), pd.Series([3.0, 0.0, 0.0])]
    
    expected_output = (expec_o_exp, expec_p_exp, expec_o_days_before, expec_p_days_before)
    output = cre.create_exposure_components(media_sheet, data_temp, dma_col, date_col, ms_dma_col, ms_date_col, ms_scaling_cols, ms_nads_col)

    assert tm.assert_almost_equal(expected_output, output)
    
def test_create_exposure_components_with_text_and_national_DMA(): 
    ms_dma_col = 'mediasheet_dma'
    dma_col = 'dma'
    ms_scaling_cols = 'mediasheet_scaling_1, mediasheet_scaling_2'
    ms_nads_col = 'mediasheet_number_of_ads'
    ms_date_col = 'mediasheet_date'
    date_col = 'date'
    
    d = {'dma' : pd.Series(['Ann Arbor', 'Boston']),
         'scaling_1_var_1' : pd.Series([0.5, 0.5]),
         'scaling_1_var_2' : pd.Series([0.6, 0.5]),
         'scaling_2_var_1' : pd.Series([0.7, 0.5]),
         'scaling_2_var_2' : pd.Series([0.8, 0.5]),
         'date' : pd.Series([pd.to_datetime('01/02/2000'), pd.to_datetime('01/05/2000')])}
    
    m = {'mediasheet_dma' : pd.Series(['Ann Arbor', 'Ann Arbor', 'Ann Arbor', 'Boston', 'National']),
         'mediasheet_scaling_1' : pd.Series(['scaling_1_var_1', 'scaling_1_var_2', 'scaling_1_var_1', 'scaling_1_var_2', 'scaling_1_var_1']),
         'mediasheet_scaling_2' : pd.Series(['scaling_2_var_1', 'scaling_2_var_2', 'scaling_2_var_1', 'scaling_2_var_2', 'scaling_2_var_1']),
         'mediasheet_number_of_ads' : pd.Series([1.0, 1.0, 1.0, 1.0, 1.0]),
         'mediasheet_date' : pd.Series([pd.to_datetime('01/01/2000'), pd.to_datetime('01/02/2000'), pd.to_datetime('01/03/2000'), pd.to_datetime('01/04/2000'), pd.to_datetime('01/01/2000')])}
    
    data_temp = pd.DataFrame(d)
    media_sheet = pd.DataFrame(m)
   
    # media_sheet['mediasheet_dma']
   
    expec_o_exp = [pd.Series([0.5*0.7*2, 0.6*0.8, 0.0]), pd.Series([0.5*0.5, 0.5*0.5, 0.0])]
    expec_p_exp = [pd.Series([0.5*0.7*2, 0.6*0.8, 0.7*0.5]), pd.Series([0.5*0.5, 0.5*0.5, 0.0])]
    expec_o_days_before = [pd.Series([1.0, 0.0, 0.0]), pd.Series([4.0, 1.0, 0.0])]
    expec_p_days_before = [pd.Series([3.0, 2.0, 1.0]), pd.Series([3.0, 0.0, 0.0])]
    
    expected_output = (expec_o_exp, expec_p_exp, expec_o_days_before, expec_p_days_before)
    output = cre.create_exposure_components(media_sheet, data_temp, dma_col, date_col, ms_dma_col, ms_date_col, ms_scaling_cols, ms_nads_col)

    assert tm.assert_almost_equal(expected_output, output)
    
def test_media_sheet_and_data_setup_with_numeric_DMA(tmpdir): #(media_sheet_loc, data_temp_loc, dma_col, date_col, ms_dma_col, ms_date_col, ms_scaling_cols, ms_nads_col, ooh_creation_flag, dependent_vars, filters)
    # import py
    # tmpdir = py.path.local()
    d = {'dma' : pd.Series([100, 200]),
         'scaling_1_var_1' : pd.Series([0.5, 0.5]),
         'scaling_1_var_2' : pd.Series([0.6, 0.5]),
         'scaling_2_var_1' : pd.Series([0.7, 0.5]),
         'scaling_2_var_2' : pd.Series([0.8, 0.5]),
         'date' : pd.Series([pd.to_datetime('01/02/2000'), pd.to_datetime('01/05/2000')]),
         'sample' : pd.Series([1, 1]),
         'dep' : pd.Series([1,2]),
         'useless' : pd.Series(['foo', 'bar'])
         }
    
    m = {'mediasheet_dma' : pd.Series([100, 100, 200, 200, 100, 100, 100, 100]),
         'mediasheet_scaling_1' : pd.Series(['scaling_1_var_1', 'scaling_1_var_2', 'scaling_1_var_1', 'scaling_1_var_2', 'scaling_1_var_1', np.nan, 'scaling_1_var_1', 'scaling_1_var_2']),
         'mediasheet_scaling_2' : pd.Series(['scaling_2_var_1', 'scaling_2_var_2', 'scaling_2_var_1', 'scaling_2_var_2', 'scaling_2_var_1', 'scaling_2_var_2', np.nan, 'scaling_2_var_2']),
         'mediasheet_number_of_ads' : pd.Series([1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0]),
         'mediasheet_date' : pd.Series([pd.to_datetime('01/01/2000'), pd.to_datetime('01/02/2000'), pd.to_datetime('01/03/2000'), pd.to_datetime('01/04/2000'), pd.to_datetime('01/01/2000'), pd.to_datetime('01/02/2000'), pd.to_datetime('01/03/2000'), np.nan]),
         'mediasheet_sample' : pd.Series([1.0, 2.0, 2.0, 2.0, 1.0, 2.0 ,2.0, 2.0])
         }
    data_temp = pd.DataFrame(d)
    media_sheet = pd.DataFrame(m)
    
#    data.fillna(0, inplace = True)
#    d_path = tmpdir.mkdir("sub_d").join("d.csv")
#    m_path = tmpdir.mkdir("sub_m").join("m.csv")
#    
#    d_path.write(data.to_csv(sep=","))
#    m_path.write(media_sheet.to_csv(sep=","))

    dma_col = 'dma'
    date_col = 'date'
    ms_dma_col = 'mediasheet_dma'
    ms_date_col = 'mediasheet_date'
    ms_scaling_cols = 'mediasheet_scaling_1, mediasheet_scaling_2'
    ms_nads_col = 'mediasheet_number_of_ads'
    dependent_vars = 'dep'
    ooh_creation_flag = False
    filters = 'sample, sample, sample'
    
    output = cre.media_sheet_and_data_setup(media_sheet, data_temp, dma_col, date_col, ms_dma_col, ms_date_col, ms_scaling_cols, ms_nads_col, ooh_creation_flag, dependent_vars, filters)
    (ms, dt) = output
    d_expec = {'dma' : pd.Series([100, 200]),
               'scaling_1_var_1' : pd.Series([0.5, 0.5]),
               'scaling_1_var_2' : pd.Series([0.6, 0.5]),
               'scaling_2_var_1' : pd.Series([0.7, 0.5]),
               'scaling_2_var_2' : pd.Series([0.8, 0.5]),
               'date' : pd.Series([pd.to_datetime('01/02/2000'), pd.to_datetime('01/05/2000')]),
               'sample' : pd.Series([1, 1]),
               'dep' : pd.Series([1,2])
               }
    
    m_expec = {'mediasheet_dma' : pd.Series([100, 100, 200, 200]),
               'mediasheet_scaling_1' : pd.Series(['scaling_1_var_1', 'scaling_1_var_2', 'scaling_1_var_1', 'scaling_1_var_2']),
               'mediasheet_scaling_2' : pd.Series(['scaling_2_var_1', 'scaling_2_var_2', 'scaling_2_var_1', 'scaling_2_var_2']),
               'mediasheet_number_of_ads' : pd.Series([2.0, 1.0, 1.0, 1.0]),
               'mediasheet_date' : pd.Series([pd.to_datetime('01/01/2000'), pd.to_datetime('01/02/2000'), pd.to_datetime('01/03/2000'), pd.to_datetime('01/04/2000')])
               }
               
    expected_output = (pd.DataFrame(m_expec), pd.DataFrame(d_expec))
    #assert p.read() == "content"
    #assert len(tmpdir.listdir()) == 1
    #assert 0
    tm.assert_frame_equal(pd.DataFrame(m_expec).sort(axis=1), ms.sort(axis=1))
    outputs_are_equal = pd.Series([tm.assert_frame_equal(out_frame.sort(axis=1), expec_frame.sort(axis=1)) for (out_frame, expec_frame) in zip(output, expected_output)])
    outputs_are_equal = pd.isnull(outputs_are_equal) # for some reason assert_frame_equals returns None instead of True
    assert all(outputs_are_equal)

    
def test_media_sheet_and_data_setup_with_numeric_and_national_DMA(tmpdir): #(media_sheet_loc, data_temp_loc, dma_col, date_col, ms_dma_col, ms_date_col, ms_scaling_cols, ms_nads_col, ooh_creation_flag, dependent_vars, filters)

    d = {'dma' : pd.Series([100, 200]),
         'scaling_1_var_1' : pd.Series([0.5, 0.5]),
         'scaling_1_var_2' : pd.Series([0.6, 0.5]),
         'scaling_2_var_1' : pd.Series([0.7, 0.5]),
         'scaling_2_var_2' : pd.Series([0.8, 0.5]),
         'date' : pd.Series([pd.to_datetime('01/02/2000'), pd.to_datetime('01/05/2000')]),
         'sample' : pd.Series([1, 1]),
         'dep' : pd.Series([1, 0]),
         'useless' : pd.Series(['foo', 'bar'])
         }
    
    m = {'mediasheet_dma' : pd.Series([100, 100, 200, 200, 100, 100, 100, 100, 'National', 'National']),
         'mediasheet_scaling_1' : pd.Series(['scaling_1_var_1', 'scaling_1_var_2', 'scaling_1_var_1', 'scaling_1_var_2', 'scaling_1_var_1', np.nan, 'scaling_1_var_1', 'scaling_1_var_2', 'scaling_1_var_1', 'scaling_1_var_2']),
         'mediasheet_scaling_2' : pd.Series(['scaling_2_var_1', 'scaling_2_var_2', 'scaling_2_var_1', 'scaling_2_var_2', 'scaling_2_var_1', 'scaling_2_var_2', np.nan, 'scaling_2_var_2', 'scaling_2_var_1', 'scaling_2_var_2']),
         'mediasheet_number_of_ads' : pd.Series([1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0]),
         'mediasheet_date' : pd.Series([pd.to_datetime('01/01/2000'), pd.to_datetime('01/02/2000'), pd.to_datetime('01/03/2000'), pd.to_datetime('01/04/2000'), pd.to_datetime('01/01/2000'), pd.to_datetime('01/02/2000'), pd.to_datetime('01/03/2000'), np.nan, pd.to_datetime('01/01/2000'), pd.to_datetime('01/02/2000')]),
         'mediasheet_sample' : pd.Series([1.0, 2.0, 2.0, 2.0, 1.0, 2.0 ,2.0, 2.0, 1.0, 2.0])
         }
    data_temp = pd.DataFrame(d)
    media_sheet = pd.DataFrame(m)

    dma_col = 'dma'
    date_col = 'date'
    ms_dma_col = 'mediasheet_dma'
    ms_date_col = 'mediasheet_date'
    ms_scaling_cols = 'mediasheet_scaling_1, mediasheet_scaling_2'
    ms_nads_col = 'mediasheet_number_of_ads'
    dependent_vars = 'dep'
    ooh_creation_flag = False
    filters = 'sample, sample, sample'
    
    output = cre.media_sheet_and_data_setup(media_sheet, data_temp, dma_col, date_col, ms_dma_col, ms_date_col, ms_scaling_cols, ms_nads_col, ooh_creation_flag, dependent_vars, filters)
    (ms, dt) = output
    d_expec = {'dma' : pd.Series([100, 200]),
               'scaling_1_var_1' : pd.Series([0.5, 0.5]),
               'scaling_1_var_2' : pd.Series([0.6, 0.5]),
               'scaling_2_var_1' : pd.Series([0.7, 0.5]),
               'scaling_2_var_2' : pd.Series([0.8, 0.5]),
               'date' : pd.Series([pd.to_datetime('01/02/2000'), pd.to_datetime('01/05/2000')]),
               'sample' : pd.Series([1, 1]),
               'dep' : pd.Series([1, 0])
               }
    
    m_expec = {'mediasheet_dma' : pd.Series([100, 100, 200, 200, 'National', 'National']),
               'mediasheet_scaling_1' : pd.Series(['scaling_1_var_1', 'scaling_1_var_2', 'scaling_1_var_1', 'scaling_1_var_2', 'scaling_1_var_1', 'scaling_1_var_2']),
               'mediasheet_scaling_2' : pd.Series(['scaling_2_var_1', 'scaling_2_var_2', 'scaling_2_var_1', 'scaling_2_var_2', 'scaling_2_var_1', 'scaling_2_var_2']),
               'mediasheet_number_of_ads' : pd.Series([2.0, 1.0, 1.0, 1.0, 1.0, 1.0]),
               'mediasheet_date' : pd.Series([pd.to_datetime('01/01/2000'), pd.to_datetime('01/02/2000'), pd.to_datetime('01/03/2000'), pd.to_datetime('01/04/2000'), pd.to_datetime('01/01/2000'), pd.to_datetime('01/02/2000')])
               }
               
    expected_output = (pd.DataFrame(m_expec), pd.DataFrame(d_expec))

    tm.assert_frame_equal(pd.DataFrame(m_expec).sort(axis=1), ms.sort(axis=1))
    outputs_are_equal = pd.Series([tm.assert_frame_equal(out_frame.sort(axis=1), expec_frame.sort(axis=1)) for (out_frame, expec_frame) in zip(output, expected_output)])
    outputs_are_equal = pd.isnull(outputs_are_equal) # for some reason assert_frame_equals returns None instead of True
    assert all(outputs_are_equal)

    
def test_media_sheet_and_data_setup_with_text_and_national_DMA(tmpdir): #(media_sheet_loc, data_temp_loc, dma_col, date_col, ms_dma_col, ms_date_col, ms_scaling_cols, ms_nads_col, ooh_creation_flag, dependent_vars, filters)
    # import py
    # tmpdir = py.path.local()
    d = {'dma' : pd.Series(['Ann Arbor', 'Boston']),
         'scaling_1_var_1' : pd.Series([0.5, 0.5]),
         'scaling_1_var_2' : pd.Series([0.6, 0.5]),
         'scaling_2_var_1' : pd.Series([0.7, 0.5]),
         'scaling_2_var_2' : pd.Series([0.8, 0.5]),
         'date' : pd.Series([pd.to_datetime('01/02/2000'), pd.to_datetime('01/05/2000')]),
         'sample' : pd.Series([1, 1]),
         'dep' : pd.Series([1, 0]),
         'useless' : pd.Series(['foo', 'bar'])
         }
    
    m = {'mediasheet_dma' : pd.Series(['Ann Arbor', 'Ann Arbor', 'Boston', 'Boston', 'Ann Arbor', 'Ann Arbor', 'Ann Arbor', 'Ann Arbor', 'National', 'National']),
         'mediasheet_scaling_1' : pd.Series(['scaling_1_var_1', 'scaling_1_var_2', 'scaling_1_var_1', 'scaling_1_var_2', 'scaling_1_var_1', np.nan, 'scaling_1_var_1', 'scaling_1_var_2', 'scaling_1_var_1', 'scaling_1_var_2']),
         'mediasheet_scaling_2' : pd.Series(['scaling_2_var_1', 'scaling_2_var_2', 'scaling_2_var_1', 'scaling_2_var_2', 'scaling_2_var_1', 'scaling_2_var_2', np.nan, 'scaling_2_var_2', 'scaling_2_var_1', 'scaling_2_var_2']),
         'mediasheet_number_of_ads' : pd.Series([1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0]),
         'mediasheet_date' : pd.Series([pd.to_datetime('01/01/2000'), pd.to_datetime('01/02/2000'), pd.to_datetime('01/03/2000'), pd.to_datetime('01/04/2000'), pd.to_datetime('01/01/2000'), pd.to_datetime('01/02/2000'), pd.to_datetime('01/03/2000'), np.nan, pd.to_datetime('01/01/2000'), pd.to_datetime('01/02/2000')]),
         'mediasheet_sample' : pd.Series([1.0, 2.0, 2.0, 2.0, 1.0, 2.0 ,2.0, 2.0, 1.0, 2.0])
         }
    data_temp = pd.DataFrame(d)
    media_sheet = pd.DataFrame(m)

    dma_col = 'dma'
    date_col = 'date'
    ms_dma_col = 'mediasheet_dma'
    ms_date_col = 'mediasheet_date'
    ms_scaling_cols = 'mediasheet_scaling_1, mediasheet_scaling_2'
    ms_nads_col = 'mediasheet_number_of_ads'
    dependent_vars = 'dep'
    ooh_creation_flag = False
    filters = 'sample, sample, sample'
    
    output = cre.media_sheet_and_data_setup(media_sheet, data_temp, dma_col, date_col, ms_dma_col, ms_date_col, ms_scaling_cols, ms_nads_col, ooh_creation_flag, dependent_vars, filters)
    (ms, dt) = output
    d_expec = {'dma' : pd.Series(['Ann Arbor', 'Boston']),
               'scaling_1_var_1' : pd.Series([0.5, 0.5]),
               'scaling_1_var_2' : pd.Series([0.6, 0.5]),
               'scaling_2_var_1' : pd.Series([0.7, 0.5]),
               'scaling_2_var_2' : pd.Series([0.8, 0.5]),
               'date' : pd.Series([pd.to_datetime('01/02/2000'), pd.to_datetime('01/05/2000')]),
               'sample' : pd.Series([1, 1]),
               'dep' : pd.Series([1, 0])
               }
    
    m_expec = {'mediasheet_dma' : pd.Series(['Ann Arbor', 'Ann Arbor', 'Boston', 'Boston', 'National', 'National']),
               'mediasheet_scaling_1' : pd.Series(['scaling_1_var_1', 'scaling_1_var_2', 'scaling_1_var_1', 'scaling_1_var_2', 'scaling_1_var_1', 'scaling_1_var_2']),
               'mediasheet_scaling_2' : pd.Series(['scaling_2_var_1', 'scaling_2_var_2', 'scaling_2_var_1', 'scaling_2_var_2', 'scaling_2_var_1', 'scaling_2_var_2']),
               'mediasheet_number_of_ads' : pd.Series([2.0, 1.0, 1.0, 1.0, 1.0, 1.0]),
               'mediasheet_date' : pd.Series([pd.to_datetime('01/01/2000'), pd.to_datetime('01/02/2000'), pd.to_datetime('01/03/2000'), pd.to_datetime('01/04/2000'), pd.to_datetime('01/01/2000'), pd.to_datetime('01/02/2000')])
               }
               
    expected_output = (pd.DataFrame(m_expec), pd.DataFrame(d_expec))
    tm.assert_frame_equal(pd.DataFrame(m_expec).sort(axis=1), ms.sort(axis=1))
    outputs_are_equal = pd.Series([tm.assert_frame_equal(out_frame.sort(axis=1), expec_frame.sort(axis=1)) for (out_frame, expec_frame) in zip(output, expected_output)])
    outputs_are_equal = pd.isnull(outputs_are_equal) # for some reason assert_frame_equals returns None instead of True
    assert all(outputs_are_equal)
#def test_create_file(tmpdir):
#    p = tmpdir.mkdir("sub").join("hello.txt")
#    p.write("content")
#    assert p.read() == "content"
#    assert len(tmpdir.listdir()) == 1
#    assert 0
#    
#    pass

def test_ooh_media_sheet_expand(): # (media_sheet, ms_date_col):
    pass

def func(x):
    return x + 1

def test_answer():
    assert func(3) == 5
    

if __name__ == '__main__':
   import pytest
   pytest.main("media_creation_unit_test.py")