# -*- coding: utf-8 -*-
"""
Created on Tue Aug 19 11:51:24 2014

@author: mir.henglin

Copyright Marketing Evolution
"""

### Example inputs. These should be commented out in the final version 
## Radio Testing
#media_sheet_loc = 'C:/Users/mir.henglin/Documents/Python Test Data/Cox M&A Variable Report - 20140620 - Radio.csv'
#data_temp_loc = 'C:/Users/mir.henglin/Documents/Python Test Data/mev11024b.txt'
#
#dma_col = 'qdmanum'
#date_col = 'date'
#
#ms_dma_col = 'DMA_Code'
#ms_date_col = 'Date'
#ms_scaling_cols = 'Station_Variable, Daypart_Variable'
#ms_nads_col = 'Number_Of_Ads'
## ooh_creation_flag = False
#
#filters = 'sample, sample, sample'
#dependent_vars = 'dep1, dep2, dep3'
#
#output_path = 'C:/Users/mir.henglin/Documents/Python Test Data/test_output.csv'

# do_decay=True

## OOH testing
#media_sheet_loc = 'C:/Users/mir.henglin/Documents/Python Test Data/Cox M&A Variable Report - 20140606 - OOH.csv'
#data_temp_loc = 'C:/Users/mir.henglin/Documents/Python Test Data/mev11024b.txt'
#filters = 'sample, sample, sample'
#dependent_vars = 'dep1, dep2, dep3'
#dma_col = 'qdmanum'
#date_col = 'date'
#
#ms_dma_col = 'DMA Code'
#ms_date_col = 'Date'
#ms_scaling_cols = 'Location Variable'
#ms_nads_col = 'Number of Ads'

# ooh_creation_flag = True

def media_creation(media_sheet_loc, data_temp_loc, dma_col, date_col, ms_dma_col, ms_date_col, ms_scaling_cols, ms_nads_col, ouput_path,
                   ooh_creation_flag=False, filters='sample', dependent_vars='sample', do_decay=True):

    ''' 
    Parameters
    ----------
    Notes
    -----
    Returns
    ------
    '''
    
    import marketingevolution.mediacreation as cre
    import marketingevolution.simplefunctions as sf
    # import pandas as pd
    
    success_message = "Success.pythonCall"
    
    (media_sheet, data) = cre.media_sheet_and_data_setup(media_sheet_loc, data_temp_loc, dma_col, date_col, ms_dma_col, ms_date_col, ms_scaling_cols, ms_nads_col, ooh_creation_flag, dependent_vars, filters)
    (obs_exposures, pro_exposures, obs_days_since, pro_days_since) = cre.create_exposure_components(media_sheet, data, dma_col, date_col, ms_dma_col, ms_date_col, ms_scaling_cols, ms_nads_col)
   
    non_dec_obs_exp = cre.create_nondecayed_exposures(obs_exposures, obs_days_since)
    non_dec_pro_exp = cre.create_nondecayed_exposures(pro_exposures, pro_days_since)    
    
    non_dec_exp = sf.column_bind([non_dec_obs_exp, non_dec_pro_exp], ignore_index=True)
    non_dec_exp.columns =  ['observed freq', 'projected freq']
    
    if do_decay:
        dec_exp = cre.create_decayed_exposures(obs_exposures, obs_days_since, dependent_vars, filters, data)
        data_to_write = sf.column_bind([non_dec_exp, dec_exp])
    else:  
        data_to_write = non_dec_exp
        
    sf.write_delimited_file(output_path, ',', data_to_write)
    print(success_message)
    pass 


# test_call
if __name__ == '__main__':
    media_creation(media_sheet_loc, data_temp_loc, dma_col, date_col, ms_dma_col, ms_date_col, ms_scaling_cols, ms_nads_col, output_path,
                   ooh_creation_flag=False, filters=filters, dependent_vars=dependent_vars, do_decay=True)
    