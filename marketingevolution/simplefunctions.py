# -*- coding: utf-8 -*-
"""
Created on Tue Sep 02 16:10:21 2014

@author: mir.henglin
"""

def column_bind(columns, **kwargs):
    ''' Binds a list of equal size pandas series or dataframes into a pandas data frame. nth series in the list becomes the nth column
    
        Parameters
        ----------
        columns : List of Pandas Series and/or pandas Dataframes
            Type of each column need not be dentical
            
        Notes
        -----
        Returns
        ------
        data_to_return : Pandas Dataframe
    '''
    import pandas as pd    
    data_to_return = pd.concat(columns, axis = 1, **kwargs)
    return(data_to_return)
 
def date_range(start_date, end_date, **kwargs):
    ''' generates a range of dates given a start and an end date. 
       
        Parameters
        ----------
        start_date : String or date-like. 
            The first date in the range.
        end_date : String or date-like 
            The last date in the range.
            
        Notes
        -----
        Returns
        ------
        date_range : Pandas Series
    '''
    import pandas as pd
    date_range = pd.date_range(start=start_date, end=end_date, **kwargs)
    return(date_range)

def string_split(string, delimiter, **kwargs):
    ''' takes string, returns list of strings split at the delimiter
    
        Parameters
        ----------
        string: string
            string to split
        delmiter: string
            string is split into  list items at every occurence of the delimiter 
            
        Notes
        -----
        Returns
        ------
        List of strings
    '''    
    list_of_split_strings = string.split(delimiter, **kwargs)
    return(list_of_split_strings)
    
def read_delimited_file(path, delimiter, **kwargs):
    ''' given a file path and delimiter, import a delimited file
        
        Parameters
        ----------
        string: string
            string to split
        delmiter: string
            string is split into  list items at every occurence of the delimiter 
            
        Notes
        -----
        Returns
        ------
        delimited_file : Pandas Dataframe
    '''
    import pandas as pd
    delimited_file = pd.read_csv(path, sep=delimiter, **kwargs)
    return(delimited_file)
    
def write_delimited_file(path, delimiter, output, **kwargs):
    ''' given a file path and delimiter, and pandas dataframe, export a delimited file.
        Parameters
        ----------
        path : string
            output file path
        delimiter : string
            what type of delimited file should the file be written as
        output : Pandas Dataframe
            Dataframe to be written
            
        Notes
        -----
        Returns
        ------
    '''
    # import pandas as pd
    output.to_csv(path, sep=delimiter, **kwargs)
    pass
    
def series_as_datetime(x, **kwargs):
    ''' convert pandas series of strings to pandas datetimes
        Parameters
        ----------
        x : pandas series
            Pandas Series of dates written as strings 
            
        Notes
        -----
        Returns
        ------
        date_timed : Series of pandas Datetime Objects
    '''
    import pandas as pd
    date_timed = x.apply(pd.to_datetime, **kwargs)
    return(date_timed)
    
def random_los():
    # genrate random list of series of all same length
    pass

def random_int_list():
    # generate list of random ints
    pass    