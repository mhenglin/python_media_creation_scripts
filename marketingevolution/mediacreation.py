# -*- coding: utf-8 -*-
"""
Created on Tue Sep 02 10:14:21 2014

@author: mir.henglin
"""
def apply_decay_rate(exposures, lags, decay_rate):
    '''Applys a decay rate to a vector of exposures and days before survey for multiple respondents.
    
    Parameters
    ----------
    exposures : List of Pandas Series
        every Series in the list corresponds to the exposures of a different respondent
    lags : List of Pandas Series
        every Series in the list corresponds to the days-before-survey-date of a different respondent
    decay_rate : float
        The per-day rate of decay of the exposures. 0 <= decay_rate <= 1.  For every day after the initial exposure, the value of an exposure is multiplied ny (1-decay). (Similar to radioactive decay)

    Notes
    -----
    Decay works like radioactive decay. Every day, there is only (1-decay_rate) of the previous day's value left
    
    exposures and lags are paired by list index (EG the $nth seires in exposures correponds to the same respondent the $nth series in lags). 
    
    The $mth value in the $nth exposures Series also corresponds to each $mth value in the $nth lags Series. 
    (EG the number of exposures at the $mth value of the $nth exposures Series occured the number at the $mth value of the $nth lags Series days before the survey)
    
    Returns
    ------
    decayed_exposes : Pandas Series
        Decayed exposure values for every respondent.

    Something Aout other functions
    ----
    '''
    import pandas as pd
    import numpy as np
    import marketingevolution.simplefunctions as sf 
    
    # Concatenate the vectors of days before and number of exposures into matrices
    exposure_matrix = sf.column_bind(exposures, ignore_index=True).transpose()
    lag_matrix = sf.column_bind(lags)
    
    ## Add a check here for empty series: Test if the number of columns in the concatenated matrix is different from the number of series in the list
    
    # Calculate decayed exposures.
    decayed_exposes = exposure_matrix.dot((1-decay_rate)**lag_matrix)
    
    # grab the values on the diagonal of this matrix. The copy() at the end is a future-proofing fix.
    decayed_exposes = np.diagonal(decayed_exposes).copy() 
    decayed_exposes = pd.Series(decayed_exposes)
    
    return(decayed_exposes)
    # This is about 15 times faster than a 'for' loop that matches lags and exposure       
#%%
def calc_decayed_exposures(exposures, lags, decay_rates):
    '''Function to calulate decayed exposures for many decay rates.
    
    Parameters
    ----------
    exposures : List of Pandas Series
        every Series in the list corresponds to the exposures of a different respondent
    lags : List of Pandas Series
        every Series in the list corresponds to the days-before-survey-date of a different respondent

    Notes
    -----
    Decay works like radioactive decay. Every day, there is only (1-decay_rate) of the previous day's value left
    
    exposures and lags are paired by list index (EG the $nth seires in exposures correponds to the same respondent the $nth series in lags). 
    
    The $mth value in the $nth exposures Series also corresponds to each $mth value in the $nth lags Series. 
    (EG the number of exposures at the $mth value of the $nth exposures Series occured the number at the $mth value of the $nth lags Series days before the survey)
    Returns
    ------
    decayed : List of Pandas Series
        Each list element corresponds to a different decay rate.
    '''
    # import pandas as pd
    
    # decay_rates = (pd.Series(range(0,100))/100)
    decayed = decay_rates.apply(lambda x: apply_decay_rate(exposures, lags, decay_rate = x))
    return(decayed)
#%%
def calc_exposure(respondent, media_sheet, ms_dma_col, dma_col, ms_scaling_cols, ms_nads_col, ms_date_col, date_col):
    '''Calculate a vector of observed and projected exposures and a vector of days-before-survey-date-that-an-exposure-occured (lags), given a respondent, a media sheet, and columns to calculate on.
    
    Parameters
    ----------
    respondent : Pandas Series
        row from data set. Should have all the columns needed to calculate exposures
    media_sheet : Pandas Dataframe
        The media sheet. Is transfomred using the information in "respondent" to calculate exposures
    ms_dma_col : string
        name of DMA column in the media sheet
    dma_col : string
        name of DMA column in "respondent"
    ms_scaling_cols : list of strings
        list of the names of the columns in the media sheet that scale the exposure. Typically, this will be columns that correspond to "daypart"
        or "day-of-week" type variables, as those variables are usually assigned a scaling fraction based on respondent habits 
        (EG a respondent views 4 out of 8 max hours on a monday, the variable that corresponds to monday will be assigned a value of 0.5)
    ms_nads_col : string
        Name of the column in the media sheet that lists the number of ads 
    ms_date_col : string
        Name of the column in the media sheet that lists the date
    date_col : string
        Name of the date column in "respondent"
        
    Notes
    -----

    Returns
    ------
    p_exp : Pandas Series
        Projected exposures
    p_days_before : Pandas Series
        days-before-survey (lags) for projected exposures 
    o_exp : Pandas Series
        Observed exposures
    o_days_before : Pandas Series
        days-before-survey (lags) for observed exposures
    '''
    
    import marketingevolution.simplefunctions as sf
    import numpy as np
    # import pandas.util.testing as tm
   
    respondent_dma = respondent[dma_col]
    
    
    if media_sheet[ms_dma_col].dtype == 'int64': # ONly numbers, no national or text
        matches_dma = (media_sheet[ms_dma_col] == respondent_dma)        
        respondent_sheet = media_sheet[matches_dma]
    else: 
        matches_dma = (media_sheet[ms_dma_col] == respondent_dma)
        matches_national = (media_sheet[ms_dma_col] == "National")
        matches_dma_or_national = [(dma|nat) for (dma, nat) in zip(matches_dma, matches_national)]
        respondent_sheet = media_sheet[matches_dma_or_national]        

    scaling_factors = [scaling_col for scaling_col in ms_scaling_cols]
    
    # Expanatory comment here
    scaling_factors = [respondent_sheet[scaling_col].apply(lambda var: respondent[var]) for scaling_col in ms_scaling_cols]
    scaling_factors = sf.column_bind(scaling_factors)
    scaling_factors = scaling_factors.product(axis = 1)
    
    respondent_sheet['exp'] = scaling_factors*respondent_sheet[ms_nads_col]

    respondent_sheet = respondent_sheet.pivot_table(values='exp', 
                            columns=ms_date_col, 
                            aggfunc=np.sum).reset_index()
    
    exp = respondent_sheet['exp']
    
    # convert to days from nanoseconds by dividing by 1 day                    
    obs_days_before = (respondent[date_col] - respondent_sheet[ms_date_col]).apply(lambda x: x/np.timedelta64(1, 'D'))
    
    # the pro_days created here is more of a placeholder right now than anything, since i dont think decay is calculated on projected exposures
    # because the numbers we recive dont take decay into account. Its used once in the funciton to get projected exposures with a decay rate of 0
    # However, with a decay rate of 0, it doesnt actually matter what the pro days since looks like.
    # I decided keep it because it doent add signifcant run time, keeps things symmetric, and maybe in the future, we will actually do some sort of calibration
    # with some sort of projected days since metric.
    
    pro_days_before = (media_sheet[ms_date_col].max() - respondent_sheet[ms_date_col]).apply(lambda x: x/np.timedelta64(1, 'D'))
                
    matches_date = (respondent_sheet[ms_date_col] <= respondent[date_col]) 
    
    p_exp = exp
    p_days_before = pro_days_before
    o_exp = exp*matches_date
    o_days_before = obs_days_before*matches_date
    
    return(o_exp, p_exp, o_days_before, p_days_before)   
#%%

def create_exposure_components(media_sheet, data, dma_col, date_col, ms_dma_col, ms_date_col, ms_scaling_cols, ms_nads_col):            
    ''' Create the components needed to calculate exposures
    
    Parameters
    ----------
    media_sheet : Pandas Dataframe
        The media sheet. Is transfomred using the information in "respondent" to calculate exposures
    data: Pandas Dataframe
        Respondent response information         
    dma_col : string
        name of DMA column in "respondent"
    date_col : string
        Name of the date column in "respondent"
    ms_dma_col : string
        name of DMA column in the media sheet
    ms_date_col : string
        Name of the column in the media sheet that lists the date
    ms_scaling_cols : list of strings
        list of the names of the columns in the media sheet that scale the exposure. Typically, this will be columns that correspond to "daypart"
        or "day-of-week" type variables, as those variables are usually assigned a scaling fraction based on respondent habits 
        (EG a respondent views 4 out of 8 max hours on a monday, the variable that corresponds to monday will be assigned a value of 0.5)
    ms_nads_col : string
        Name of the column in the media sheet that lists the number of ads
    
    Notes
    -----
    Before Running this function, make sure all columns (sample, depvars, dma, date, all media vars in the media sheet are columns in the dataset)
    
    Returns
    ------
    pro_exposures : List of Pandas Series
        Projected exposures for all respondents
    pro_days_before : List of Pandas Series
        days-before-survey (lags) for projected exposures for all respondents
    obs_exposures : List of Pandas Series
        Observed exposures for all respondents
    obs_days_before : List of Pandas Series
        days-before-survey (lags) for observed exposures for all respondents
    
    '''
    import time
    import marketingevolution.simplefunctions as sf
    t1 = time.time()
    delimiter = ', '
    ms_scaling_cols = sf.string_split(ms_scaling_cols, delimiter)
    success_message = 'Are you seeing this message? Then a great victory is yous! Components Complete!'
    
    obs_exposures = []
    obs_days_before = []
    pro_exposures = []
    pro_days_before = []
    
    
    for i, respondent in data.iterrows(): # i = 0 1611, respondent = data.iloc[i] 
        (o_exp, p_exp, o_days_before, p_days_before) = calc_exposure(respondent, media_sheet, ms_dma_col, dma_col, ms_scaling_cols, ms_nads_col, ms_date_col, date_col)
        
        pro_exposures.append(p_exp)
        pro_days_before.append(p_days_before)
        obs_exposures.append(o_exp)
        obs_days_before.append(o_days_before)


    # Adjust series for each respondent so that they are the same length. This is important for vectorizing decay calculations.
    (obs_exposures, pro_exposures, obs_days_before, pro_days_before) = homogenize_series_length(obs_exposures, pro_exposures, obs_days_before, pro_days_before)
    
    t2 = time.time()
    
    print('It took %f seconds to create the components' % (t2 - t1))
    
    print(success_message)

    return((obs_exposures, pro_exposures, obs_days_before, pro_days_before))
#%%
def homogenize_series_length(obs_exposures, pro_exposures, obs_days_before, pro_days_before):
    ''' Takes 4 lists of Pandas Series with numeric data and adds zeros to shorter series until all series are the same length
    Parameters
    ----------
    obs_exposures, pro_exposures, obs_days_before, pro_days_before : Lists of pandas series
    
    Notes
    -----
    This function is janky as heck. Feel free to improve it
    
    Returns
    ------
    
    obs_exposures, pro_exposures, obs_days_before, pro_days_before : Lists of pandas series
    '''
    import pandas as pd
    import numpy as np
    #This function is janky as all heck. Feel free to improve it
    
    # Get the length of the longest series
    which_empty = [respondent.empty for respondent in obs_exposures]
    not_empty = [respondent for (respondent, empty) in zip(obs_exposures, which_empty) if not empty]
    series_length = [len(respondent) for respondent in not_empty]
    series_length = np.max(series_length)
    
    # Make all series the same length by adding zeros
    obs_exposures =  [respondent.append(pd.Series(np.zeros(series_length - len(respondent)))) for respondent in obs_exposures]    
    pro_exposures =  [respondent.append(pd.Series(np.zeros(series_length - len(respondent)))) for respondent in pro_exposures]    
    obs_days_before =  [respondent.append(pd.Series(np.zeros(series_length - len(respondent)))) for respondent in obs_days_before]    
    pro_days_before =  [respondent.append(pd.Series(np.zeros(series_length - len(respondent)))) for respondent in pro_days_before] 

    # adjust index so every row is unique
    obs_exposures =  [respondent.reset_index(drop=True) for respondent in obs_exposures]    
    pro_exposures =  [respondent.reset_index(drop=True) for respondent in pro_exposures]    
    obs_days_before =  [respondent.reset_index(drop=True) for respondent in obs_days_before]    
    pro_days_before =  [respondent.reset_index(drop=True) for respondent in pro_days_before] 
    return((obs_exposures, pro_exposures, obs_days_before, pro_days_before))

#%%
def media_sheet_and_data_setup(media_sheet, data_temp, dma_col, date_col, ms_dma_col, ms_date_col, ms_scaling_cols, ms_nads_col, ooh_creation_flag, dependent_vars, filters):              
    ''' Import and prepare a dataset and media sheet for use in media creations.
   
    
    Parameters
    ----------
    media_sheet : Pandas Dataframe
        media sheet
    data_loc: Pandas Dataframe
        data
    dma_col : string
        name of DMA column in "respondent"
    date_col : string
        Name of the date column in "respondent"
    ms_dma_col : string
        name of DMA column in the media sheet
    ms_date_col : string
        Name of the column in the media sheet that lists the date
    ms_scaling_cols : list of strings
        list of the names of the columns in the media sheet that scale the exposure. Typically, this will be columns that correspond to "daypart"
        or "day-of-week" type variables, as those variables are usually assigned a scaling fraction based on respondent habits 
        (EG a respondent views 4 out of 8 max hours on a monday, the variable that corresponds to monday will be assigned a value of 0.5)
    ms_nads_col : string
        Name of the column in the media sheet that lists the number of ads
    ooh_creation_flag : bool
        Is the creation an OOH creation
    dependent_vars : string
        a single string listing depvars to be used for decay testing. Each depvar should be delimited by ", "
    filters : string
        a single string listing filters that a respondent must match to be used ind ecay testing. Each filter should be delimited by ", "
        
    Notes
    -----
    
    Returns
    ------
    media_sheet_2 : Pandas Dataframe
        prepared media sheet
    data_temp : Pandas Dataframe
        prepared dataset
        
    Helper Functions
    ----------------
    mediacreation:
        ooh_media_sheet_expand
    simplefunctions:
        read_delimited_file
        string_split
    '''

    import time
    import pandas as pd
    import random
    import numpy as np
    import marketingevolution.simplefunctions as sf

    delimiter = ', '
    ms_scaling_cols = sf.string_split(ms_scaling_cols, delimiter)
    filters = sf.string_split(filters, delimiter)
    dependent_vars = sf.string_split(dependent_vars, delimiter)

    t1 = time.time()

    # The low_memory argument basically allows python to use more memory to suss out things it finds weird. Also it makes things run faster (I think)
    # media_sheet = sf.read_delimited_file(media_sheet_loc, ',', low_memory = False)
    # data_temp = sf.read_delimited_file(data_loc, ',', low_memory = False)  # delimiter might also be '|'
    
    success_message = 'Are you seeing this message? Then a great victory is yous! Setup Complete!'

    if ooh_creation_flag == True:
        media_sheet = ooh_media_sheet_expand(media_sheet, ms_date_col)
        
    # Remove from the media sheet rows with null scaling, Date, or DMA because having valid entrie in those columns are the minimum necessary criteria for media creation. 
    media_sheet.dropna(how='any', subset=[ms_date_col, ms_dma_col]+ms_scaling_cols, inplace=True)
 
    media_sheet[[ms_date_col]] = sf.series_as_datetime(media_sheet[[ms_date_col]])
    data_temp[date_col] = sf.series_as_datetime(data_temp[date_col])
    
    # data_temp[dma_col].fillna(0, inplace = True)

    # Comment here explaining what this does
    scaling_vars = [pd.Series(media_sheet[scaling_col].unique()) for scaling_col in ms_scaling_cols]
    scaling_vars = pd.concat(scaling_vars).tolist()
    #%%
    ###################################################################################################
    # Create the sample column. Comment out in final version
    data_temp['sample'] = 1
       
    # Add depdent vars to dataset for testing purposes. Comment out final version
    data_temp['dep1'] = pd.Series(random.randint(0,1) for n in range(len(data_temp['sample'])))
    data_temp['dep2'] = pd.Series(random.randint(0,1) for n in range(len(data_temp['sample'])))
    data_temp['dep3'] = pd.Series(random.randint(0,1) for n in range(len(data_temp['sample'])))
    
    # This line exists for testing purposes only. All NAs should be handled in SPLUS. Comment out in final version.
    data_temp.fillna(0, inplace = True)
    ####################################################################################################
    #%%
    # Grab only the necessary columns from the dataframe, throwing away the rest. 
    data_temp_cols_to_grab = np.unique(['sample', dma_col, date_col]+scaling_vars+filters+dependent_vars)
    data_temp = data_temp[data_temp_cols_to_grab]
    #%%
       
    array = media_sheet.pivot_table(values=ms_nads_col, 
                                    columns=[ms_dma_col, ms_date_col]+ms_scaling_cols, 
                                    aggfunc=np.sum)

    media_sheet_2 = array.reset_index()
    media_sheet_2.columns = [ms_dma_col, ms_date_col]+ms_scaling_cols+[ms_nads_col]


    t2 = time.time()
    
    print('It took %f seconds to setup the media sheet and data' % (t2 - t1))
    
    print(success_message)
    
    ### return or export or what do i do here?
    return((media_sheet_2, data_temp))

#%%
def ooh_media_sheet_expand(media_sheet, ms_date_col, ms_start_date_col='Start.Date', ms_end_date_col='End.Date'):
    ''' A function used to generate additional rows based on the Start and End Date of a given row
    It calculates the number of days between the start and end date. Creates a date vector containing a date for each of those days
    and then duplicates the row for every date in the date vector. Finally it concatenates everything into a single dataframe
    
    Parameters
    ----------
    media_sheet : Pandas Dataframe
        The media sheet. Should contain a column named 'Start Date' and one named 'End Date'
    ms_date_col : string
        The name for the newly created date column
        
    Notes
    -----
    For use only in the case of an OOH creation
    
    Returns
    ------    
    media_sheet : Pandas Dataframe
    '''
    import pandas as pd
    import marketingevolution.simplefunctions as sf
    dfs = []
    
    for i, row in media_sheet.iterrows():
        # ms_start_date_col = 'Start Date'
        # ms_end_date_col = 'End Date'
        
        row_start_date = row[ms_start_date_col] 
        row_end_date = row[ms_end_date_col]
        index = sf.date_range(row_start_date, row_end_date)
        rows = pd.DataFrame([row.copy() for date in index])
        
        # add the date column in
        rows[ms_date_col] = index
        rows = rows.reset_index(drop=True)        
        dfs.append(rows)
    
    media_sheet = pd.concat(dfs)
    return media_sheet

#%%
def test_for_best_decay(decayed_exp, dep, filter_set):
    '''Function to calculate which decay rate is best decay rate. 
    Takes in a vector of exposures that have already had decay applied to them, 
    a depedent variable to optimize on, and a filter set to specify the target that should be used in the optimization.
    
    Grabs a vector of exposures for which decay has already been calculated.
    Grabs the respondents from that vector that matches the filter set.
    Runs logit regression of those exposures on the depvar.
    Grabs and stores the deviance.
    
    Calculates the minimum deviance from the stored deviances, and returns the decayed exposures which correpsonds to the minimum deviance.
    
    Parameters
    ----------
    decayed_exp : List of Pandas Series
        The decay calculated exposures that will be used for the calculation
    dep : Pandas Series
        Response variable to model against
    filter_set : Pandas Dataframe
        Filter set that a respondent must match in order to be inlcuded in modeling. Each column corresponds toa  filter. Respondent must match all filters
        
    Notes
    -----
    
    Returns
    ------
    min_deacyed_exposures : Pandas Series
        Decayed Exposure that corresponds to minimum deviance
    '''
    import pandas as pd
    # import statsmodels.api as sm   
    
    decay_rates = (pd.Series(range(0,100))/100)
    matches_all_filters = filter_set.all(axis=1)
    deviances = []
    for i in range(len(decayed_exp)): # i = 1
        # print(i)        
        exp = decayed_exp.iloc[i]
        # NB Filter np arrays with other np arrays
        exp = exp[matches_all_filters.values] 
        
        logit_fit = fit_logit_model(exp, dep)
        deviances.append(get_model_deviance(logit_fit))
        null_deviance = get_null_deviance(logit_fit)

    deviances = pd.Series(deviances)
    min_deviance = min(deviances) 
    ## The [0s] at the end of this probably has a more elegent solution. Basically I want to grab the list object, not return a list. So I create a one-element list and index into it
    min_decay_rate = [decay_rates[i] for i in range(len(decay_rates)) if (deviances[i] == min_deviance)][0]
    min_decayed_exposures = [decayed_exp.iloc[i] for i in range(len(decayed_exp)) if (deviances[i] == min_deviance)][0]
    min_decayed_exposures = pd.Series(min_decayed_exposures)
    print('Chosen decay rate is %f, null deviance is %f and deviance with decay is %f' % (min_decay_rate, null_deviance, min_deviance))
    with open('chosen_decay_rate.txt', 'w') as ff:
        ff.write(str(min_decay_rate))
    return(min_decayed_exposures)
    
#%%
def create_nondecayed_exposures(exposures, lags):       
    ''' Creates frequency from exposure components. Calculates non decayed exposure values by applying a decay rate of 0 to the exposure components.
    
    This essentailly boils down to summing up the values of each Series  on the exposures lists.
    
    Parameters
    ----------
    exposures : List of Pandas Series
    lags : List of Pandas Series
    
    Notes
    -----
    exposures and lags are paired by list index (EG the $nth seires in exposures correponds to the same respondent the $nth series in lags). 
    The $mth value in the $nth exposures Series also corresponds to each $mth value in the $nth lags Series. 
    (EG the number of exposures at the $mth value of the $nth exposures Series occured the number at the $mth value of the $nth lags Series days before the survey)    
    
    Returns
    ------    
    freq : Pandas Series
    '''

    #%%
    import pandas as pd
    import time
    #%%
    t1 = time.time()
    success_message = 'Are you seeing this message? Then a great victory is yous! Have some non-decayed media!'
    #%%
    
    # freq = apply_decay_rate(exposures, lags, decay_rate=0)        
    freq = pd.Series([respondent.sum() for respondent in exposures])    
    t2 = time.time()
    
    print('It took %f seconds to run this non-decayed creation' % (t2 - t1))
    print(success_message)
    
    return(freq)
    
#%%
def create_decayed_exposures(exposures, lags, dependent_vars, filters, data):       
    '''Applys a decay rate to a vector of exposures and days before survey for multiple respondents.
    
    Parameters
    ----------
    exposures : List of Pandas Series
        every Series in the list corresponds to the exposures of a different respondent
    lags : List of Pandas Series
        every Series in the list corresponds to the days-before-survey-date of a different respondent
    dependent_vars : string
        a single string listing depvars to be used for decay testing. Each depvar should be delimited by ", "
    filters : string
        a single string listing filters that a respondent must match to be used in decay testing. Each filter should be delimited by ", "
    data : Pandas Dataframe
        respondent info. Should contain the columns listed in filters and dependent_vars.

    Notes
    -----
    Decay works like radioactive decay. Every day, there is only (1-decay_rate) of the previous day's value left
    
    exposures and lags are paired by list index (EG the $nth seires in exposures correponds to the same respondent the $nth series in lags). 
    
    The $mth value in the $nth exposures Series also corresponds to each $mth value in the $nth lags Series. 
    (EG the number of exposures at the $mth value of the $nth exposures Series occured the number at the $mth value of the $nth lags Series days before the survey)
    
    Returns
    ------
    decayed_freqs : Pandas Series
        Decayed exposure values for every respondent.
    '''
    
    #%%
    import pandas as pd
    import time
    #%%
    t1 = time.time()
    delimiter = ', '
    filters = filters.split(delimiter)
    dependent_vars = dependent_vars.split(delimiter)
    #%%
    success_message = 'Are you seeing this message? Then a great victory is yous! Have some decayed media!'

    decayed_exposures = calc_decayed_exposures(exposures, lags, decay_rates = (pd.Series(range(0,100))/100))
    #%%
    list_for_holding_decayed_exposures = []
    for depvar in dependent_vars: #depvar = 'dep1'
        print(depvar)
        optimal_decayed_exposure = test_for_best_decay(decayed_exp=decayed_exposures, dep=data[depvar], filter_set=data[filters]) 
        list_for_holding_decayed_exposures.append(optimal_decayed_exposure)

    #%%
    decayed_freqs = pd.concat(list_for_holding_decayed_exposures, axis = 1, ignore_index = True)
    decayed_freqs.columns = ['decayed obs freq ' + depvar for depvar in dependent_vars]
    
    t2 = time.time()
    
    print('It took %f seconds to run this decayed creation' % (t2 - t1))
    print(success_message)

    return(decayed_freqs)
    
#%%
def fit_logit_model(x, dep):
    import statsmodels.api as sm
    ''' Fit a logisitc regression given a x amtrix and dependent variable
    Parameters
    ----------
     x : Pandas Dataframe?
         The predictor variable. Each column corresponds toa  different predictor. Column of 1 for the intercept should NOT yet be added
     dep : Pandas series?
         Vector of response. Should be binary (1 or 0)
    Notes
    -----
    Returns
    ------
    Statsmodels logist Fit
    '''
    # Add in column of 1s for intercept
    x = sm.add_constant(x) 
    logit = sm.Logit(dep, x)
    logit_fit = logit.fit()
    return(logit_fit)
    
#%%
def get_model_deviance(logit_fit):
    ''' 
    Parameters
    ----------
    logit_fit : Statsmodels Logit Fit
    
    Notes
    -----
    
    Returns
    ------
    deviance : float
        model deviance
    '''
    log_likelihood = logit_fit.llf
    deviance = -2*log_likelihood
    return(deviance)
#%%
def get_null_deviance(logit_fit):
    ''' 
    Parameters
    ----------
    logit_fit : Statsmodels Logit Fit
    
    Notes
    -----
    
    Returns
    ------
    null_deviance : float
        null deviance
    '''
    null_log_likelihood = logit_fit.llf
    null_deviance = -2*null_log_likelihood
    return(null_deviance)
    

    
